<?php


namespace App\Services;


use App\User;

class ExampleService extends CrudService
{
    public function __construct()
    {
        $this->resourceName = User::class;
    }
}
