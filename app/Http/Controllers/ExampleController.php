<?php


namespace App\Http\Controllers;

use App\Http\Requests\ExampleRequest;

class ExampleController extends CrudController
{
    public function __construct()
    {
        $this->serviceClassName = CrudController::class;
        $this->requestFormClassName = ExampleRequest::class;
        parent::__construct();
    }
}
