<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */
$router->group(['middleware' => 'auth'], function () use ($router) {

    $router->group(['prefix' => '/'], function () use ($router) {
        $router->get('/', function () {
            return response('', 200);
        });
        $router->get('{id}', function () use ($router) {
            return response('', 200);
        });
        $router->post('', function () use ($router) {
            return response('', 201);
        });
        $router->put('{id}', function () use ($router) {
            return response('', 200);
        });
        $router->delete('{id}', function () use ($router) {
            return response('', 204);
        });
    });

});

$router->post('/auth', 'AuthController@buildToken');
